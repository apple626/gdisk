Name:    gdisk
Version: 1.0.5
Release: 6
Summary: GPT fdisk(consisting of the gdisk,sgdisk,cgdisk) is a set of text-mode partitioning tools
License: GPLv2
URL:     http://www.rodsbooks.com/gdisk
Source0: http://downloads.sourceforge.net/gptfdisk/gptfdisk-%{version}.tar.gz

Patch1:	 0001-Fix-segfault-on-some-weird-data-structures.patch


BuildRequires:ncurses-devel util-linux-devel gcc-c++ popt-devel 

%description
GPT fdisk(consisting of the gdisk,sgdisk,cgdisk) is a set of text-mode partitioning tools.
rather than on the older MBR partition tables.

%package help
Summary:   Include man page for gdisk,sgdisk,cgdisk utility
Requires:  man

%description help
This package contains the man page for GPT fdisk(consisting of the gdisk,sgdisk,cgdisk)

%prep
%autosetup -n gptfdisk-%{version} -p1

%build
make CXXFLAGS="%{optflags} -D_FILE_OFFSET_BITS=64" LDFLAGS="%{build_ldflags}"

%check
make test
chmod 0644 gdisk_test.sh

%install
install -Dp -m 0755 cgdisk %{buildroot}%{_sbindir}/cgdisk
install -Dp -m 0644 cgdisk.8 %{buildroot}%{_mandir}/man8/cgdisk.8
install -Dp -m 0755 gdisk %{buildroot}%{_sbindir}/gdisk
install -Dp -m 0644 gdisk.8 %{buildroot}%{_mandir}/man8/gdisk.8
install -Dp -m 0755 sgdisk %{buildroot}%{_sbindir}/sgdisk
install -Dp -m 0644 sgdisk.8 %{buildroot}%{_mandir}/man8/sgdisk.8
install -Dp -m 0755 fixparts %{buildroot}%{_sbindir}/fixparts
install -Dp -m 0644 fixparts.8 %{buildroot}%{_mandir}/man8/fixparts.8

%files
%doc gdisk_test.sh README NEWS
%license COPYING
%{_sbindir}/*

%files help
%{_mandir}/man8/*

%changelog
* updated at 2021-11-05 14:44:47 by apple626
- add logs

* Fri Jul 30 2021 chenyanpanHW <chenyanpan@huawei.com> - 1.0.5-6
- DESC: delete -Sgit from %autosetup, and delete BuildRequires git

* Fri Jul 23 2021 zhouwenpei <zhouwenpei1@huawei.com> - 1.0.5-5
- remove unnecessary build require.

* Wed Nov 4 2020 lixiaokeng <lixiaokeng@huawei.com> - 1.0.5-4
- add make test

* Thu Oct 29 2020 Zhiqiang Liu <liuzhiqiang26@huawei.com> - 1.0.5-3
- backport one patch for solving potential segfault problem.

* Mon Aug 10 2020 volcanodragon <linfeilong@huawei.com> - 1.0.5-2
- upgrade yaml

* Thu Jul 16 2020 Wangjun <wangjun196@huawei.com> - 1.0.5-1
- upgrade package to 1.0.5

* Wed Jan 22 2020 sunshihao <sunshihao@huawei.com> - 1.0.4.5
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:update buildrequire

* Tue Aug 27 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.0.4.4
- Created spec file for 1.0.4.4 release
#append test 2021-12-01 17：02
#append comment 2021-12-06 13:26
